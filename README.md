# README #

This is a web-based note-taking tool for language learners. 
In this projects there is a backend made in django and a frontend made in React js. 
You can see how to connect React js to a "django REST server web".

## How to install it? ##


### Using Docker ###

```
docker build -t note-backend backend/.
docker build -t note-frontend frontend/.


docker run -d -p 8000:8000 note-backend
docker run -d -p 8083:8083 note-frontend
```

### Without Docker ###


Go to 'backend' the directory
```
cd backend
```

Create a new Virtualenv, Python 3 [(install virtualenv)](https://virtualenv.pypa.io/en/stable/installation/).

Activate it, install requirements and migrate the db

```
pip install -r requirements.txt
python manage.py migrate
```

and run server web 
```
python manage.py runserver 0.0.0.0:8000
```

Then, frontend!

go to 'frontend' the directory
```
cd frontend
```
Install and run
```
npm install
npm run
```

### How it works ###

I created a starter db with a superuser;

superuser's credential
```
username: memrise
password: covacev01
```
Here
http://127.0.0.1:8000/admin/core/language/
you'll be able to add new Languages.

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact

