# Deliverables #

### How to run the web application and/or a publicly accessible deployment. ###
  See README.md


### Any thoughts you have about the challenge, in particular ###

* Tradeoffs you made to balance delivery time and feature completeness + what
improvements you might have madeif you had unlimited time and resources ((i.e. todo list))


    1. to fix Autocomplete Language (the list has an issue when it's selected)
    and to move it  as Component.

    2. in frontend, to add alerts to catch exceptions like "server offline" to have a better user experience.

    3. to add a "logical delete" to objects and to fix the size of fields.

    4. to modify dockers introducing web servers intead of commands.

    5. to confirm registration with sending email (omitted to speed up the experience).

    6. to introduce other services as sentry to have more control.

    7. to increase the package 'wordreferences', obtaining more data from webservice, etc.


* How you would scale the application to > 1m daily active users


    1. I'd insert caching (example memcached, varnish) to minify selects to the db.

    2. Probably, I'd create a scripts to populate the package 'wordreferences';
    this will avoid calls to external webservice.

    3. Elasticsearch could be a good alternative to make more performant selects.

    4. I'd work to databases, creating replications, sharding table for language and/or user

    5. Regarding web servers, I'd use docker to scale them.

Infact, I think that the main problems could be the database and the calls
to external webservices.