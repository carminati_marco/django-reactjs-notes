from django.contrib.auth.models import AbstractUser
from django.db import models


class Language(models.Model):
    """
    Language Code; this will be a key for note.
    """
    code = models.CharField(max_length=8, default='')
    name = models.TextField(default='')

    def __str__(self):
        return self.name

    class Meta:
        ordering = ['name']


class CustomUser(AbstractUser):
    """
    Extend AbstractUser.
    This permits to override REGISTER_SERIALIZER_CLASS with less effort.
    NOTE: for other extra data create Profile models with onetoone field.
    """
    email = models.EmailField(max_length=254, unique=True)
    first_name = models.TextField(default='')
    last_name = models.TextField(default='')
    language = models.ForeignKey(Language, on_delete=models.SET_NULL, blank=True, null=True)
