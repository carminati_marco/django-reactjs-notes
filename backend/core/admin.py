from django.contrib import admin

from .models import Language, CustomUser

"""
Admin section.
"""

admin.site.register(Language)
admin.site.register(CustomUser)

