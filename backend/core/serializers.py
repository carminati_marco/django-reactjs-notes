from rest_framework import serializers

from .models import CustomUser


class UserSerializer(serializers.ModelSerializer):
    """
    Serializer used to manage customer (ex. Registration).
    """
    class Meta:
        model = CustomUser
        fields = ('email', 'username', 'password', 'language', 'first_name', 'last_name')

    def create(self, validated_data):
        user = super(UserSerializer, self).create(validated_data)
        user.set_password(validated_data['password'])
        user.save()
        return user
