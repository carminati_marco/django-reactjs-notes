from django.contrib import admin

from .models import LanguageUser, Note

"""
Admin section.
"""

admin.site.register(LanguageUser)
admin.site.register(Note)
