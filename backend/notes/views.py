# Create your views here.
from django.http import Http404
from rest_framework import authentication, permissions, status
from rest_framework.response import Response
from rest_framework.views import APIView
from wordreferences.utils import get_word_reference

from .models import Language, Note, LanguageUser
from .serializers import LanguageSerializer, NoteSerializer, LanguageUserSerializer


class LanguageList(APIView):
    """
    Simply Serializer with Language object.
    """
    def get(self, request, format=None):

        # get languages and serialize.
        languages = Language.objects.all()
        serializer = LanguageSerializer(languages, many=True)
        return Response(serializer.data)


class NoteList(APIView):
    """
    List all notes, or create a new note.

    * Requires token authentication.
    * Only authenticated users are able to access this view.
    """
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)

    def get(self, request, format=None):
        """
        Get language_users + notes for users.
        """
        language_users = LanguageUser.objects.filter(user=request.user)
        serializer = LanguageUserSerializer(language_users, many=True)
        return Response(serializer.data)

    def post(self, request, format=None):
        """
        Create a note
        """
        data = request.data
        serializer = NoteSerializer(data=request.data)
        if serializer.is_valid():

            # obtain relative LanguageUser.
            language = Language.objects.get(id=data['language'])
            language_user, created = LanguageUser.objects.get_or_create(user=request.user, language=language)

            # check if note exists.
            if Note.objects.filter(language_user_id=language_user, word=data['word']).exists():
                note = Note.objects.get(language_user_id=language_user, word=data['word'])
            else:
                note = serializer.save()
                note.language_user = language_user

            # try to tranlsate the word.
            note.word_reference = get_word_reference(note.word, language, request.user.language)
            note.save()
            # return back.
            return Response(serializer.data, status=status.HTTP_201_CREATED)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class NoteDetail(APIView):
    """
    Manage Note models.

    * Requires token authentication.
    * Only authenticated users are able to access this view.
    """
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)

    def get_object(self, pk, user):
        try:
            return Note.objects.get(pk=pk, language_user__user=user)
        except Note.DoesNotExist:
            raise Http404

    def get(self, request, pk):
        """
        Get Note with pk.
        """
        note = self.get_object(pk, request.user)
        serializer = NoteSerializer(note)
        return Response(serializer.data)

    def put(self, request, pk, format=None):
        """
        Update
        """
        note = self.get_object(pk, request.user)
        serializer = NoteSerializer(note, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk, format=None):
        """
        Delete relative Note
        """
        note = self.get_object(pk, request.user)

        # get language_user and delete.
        language_user = note.language_user
        note.delete()

        # if I haven't note anymore, I'll delete the language_user
        if not language_user.notes.all().exists():
            language_user.delete()

        return Response(status=status.HTTP_204_NO_CONTENT)
