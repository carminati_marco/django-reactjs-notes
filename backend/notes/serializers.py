from rest_framework import serializers

from .models import Language, LanguageUser, Note

"""
Serializer to manage the package.
"""

class LanguageSerializer(serializers.ModelSerializer):

    class Meta:
        model = Language
        fields = ('pk', 'name')


class NoteSerializer(serializers.ModelSerializer):

    word_reference = serializers.StringRelatedField(many=False)

    class Meta:
        model = Note
        fields = ('pk', 'word', 'definition', 'context', 'language', 'word_reference')


class LanguageUserSerializer(serializers.ModelSerializer):

    language = serializers.StringRelatedField(many=False)
    notes = NoteSerializer(many=True, read_only=True)

    class Meta:
        model = LanguageUser
        fields = ('notes', 'language', 'user')
