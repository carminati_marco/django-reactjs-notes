from core.models import Language
from django.conf import settings
from django.db import models
from wordreferences.models import WordReference

User = settings.AUTH_USER_MODEL


class LanguageUser(models.Model):
    """
    Object created for create a list Language for each User.
    """
    user = models.ForeignKey(User, on_delete=models.SET_NULL, blank=True, null=True)
    language = models.ForeignKey(Language, on_delete=models.SET_NULL, blank=True, null=True)

    def __str__(self):
        return '%(user)s - %(language)s' % dict(user=self.user, language=self.language)


class Note(models.Model):
    """
    This class contain note unique for language_user/word.
    """
    language_user = models.ForeignKey(LanguageUser, related_name='notes', on_delete=models.SET_NULL, blank=True,
                                      null=True)
    word = models.TextField()
    definition = models.TextField()
    context = models.TextField(default='')
    word_reference = models.ForeignKey(WordReference, related_name='notes', on_delete=models.SET_NULL, blank=True,
                                       null=True)

    @property
    def language(self):
        return str(self.language_user.language)

    def __str__(self):
        return '[%(language_user)s] %(word)s' % dict(language_user=self.language_user, word=self.word)

    class Meta:
        ordering = ['word']
        unique_together = ('language_user', 'word')
