import json

from django.urls import reverse

from rest_framework.authtoken.models import Token
from rest_framework.test import APITestCase
from core.models import CustomUser as User, Language
from notes.models import Note, LanguageUser
from notes.serializers import NoteSerializer


class NoteListCreateAPIViewTestCase(APITestCase):
    url = reverse("notes:list")

    def setUp(self):
        self.username = "marco"
        self.email = "marco@uau.it"
        self.password = "you_know_nothing"
        self.user = User.objects.create_user(self.username, self.email, self.password,
                                             language=Language.objects.create(code="eng", name="English"))
        self.language = Language.objects.create(code="ita", name="Italian")
        self.language_user = LanguageUser.objects.create(language=self.language, user=self.user)
        self.token = Token.objects.create(user=self.user)
        self.api_authentication()

    def api_authentication(self):
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + self.token.key)

    def test_create_note(self):
        print(self.language.id)
        response = self.client.post(self.url, {"word": "WORD", "definition": "DEFINITION", "language": self.language.id})
        self.assertEqual(201, response.status_code)

    def test_user_notes(self):
        """
        Test to verify user notes list
        """
        Note.objects.create(language_user=self.language_user, word="word", definition="definition")
        response = self.client.get(self.url)
        self.assertTrue(len(json.loads(response.content)) == Note.objects.count())


class NoteDetailAPIViewTestCase(APITestCase):

    def setUp(self):
        self.username = "marco"
        self.email = "marco@uau.it"
        self.password = "you_know_nothing"
        self.user = User.objects.create_user(self.username, self.email, self.password,
                                        language=Language.objects.create(code="eng", name="English"))


        self.language = Language.objects.create(code="ita", name="Italian")
        self.language_user = LanguageUser.objects.create(language=self.language, user=self.user)
        self.note = Note.objects.create(language_user=self.language_user, word="word", definition="definition")
        self.url = reverse("notes:detail", kwargs={"pk": self.note.pk})
        self.token = Token.objects.create(user=self.user)
        self.api_authentication()


    def api_authentication(self):
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + self.token.key)

    def test_note_object_bundle(self):
        """
        Test to verify note object bundle
        """
        response = self.client.get(self.url)
        self.assertEqual(200, response.status_code)

        note_serializer_data = NoteSerializer(instance=self.note).data
        response_data = json.loads(response.content)
        self.assertEqual(note_serializer_data, response_data)


    def test_note_object_update(self):
        self.client.put(self.url, {"definition": "It's Jess!"})
        response = self.client.get(self.url)
        response_data = json.loads(response.content)
        note = Note.objects.get(id=self.note.id)
        self.assertEqual(response_data.get("definition"), note.definition)



    def test_note_object_delete_authorization(self):
        """
            Test to verify that put call with different user token
        """

        new_user = User.objects.create_user('new_user', 'new_user@user.it', 'password', language=self.language)
        new_token = Token.objects.create(user=new_user)
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + new_token.key)
        response = self.client.delete(self.url)
        self.assertEqual(404, response.status_code)

    def test_note_object_delete(self):
        response = self.client.delete(self.url)
        self.assertEqual(204, response.status_code)