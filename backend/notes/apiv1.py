from django.conf.urls import url
from django.urls import path

from .views import LanguageList, NoteList, NoteDetail

"""
Pattern for API v. 1.
"""

app_name = 'notes'
urlpatterns = [
    path(r'languages/', LanguageList.as_view(),),
    path(r'notes/', NoteList.as_view(), name="list"),
    url(r'notes/(?P<pk>[0-9]+)/', NoteDetail.as_view(), name="detail"),
]

