import uuid

import requests

from .constants import API_URL
from .models import WordReference


def _create_word_reference(from_language, to_language, word, uuid):
    """
    With the parameters, the function create a word reference obtaining data from
    API_URL (glosbe.com).
    """

    # create url and call via request GET.
    url = API_URL % dict(from_code=from_language.code, to_code=to_language.code, word=word)
    response = requests.get(url)

    if response.status_code == requests.codes.ok:

        # get json and check if 'tuc' is present
        response = response.json()
        if len(response['tuc']) > 0:
            tuc = response['tuc'][0]
            word_reference, created = WordReference.objects.get_or_create(word=word,
                                                                          language=to_language,
                                                                          uuid=uuid)
            # get word + first_meaning and save.
            try:
                word_reference.word = tuc['phrase']['text']
                word_reference.context = tuc['meanings'][0]['text']
            except Exception as e:
                # todo: implement raise.
                print(e)
            word_reference.save()

            return word_reference
    else:
        return None


def get_word_reference(word, from_language, to_language):
    """
    Obtain relative word_reference giving the word and from + to languages.
    """

    word = word.lower()  # force to lowercase

    # get word_reference 'FROM LANGUAGE'
    from_word_reference, created = WordReference.objects.get_or_create(word=word, language=from_language)
    if created:
        from_word_reference.uuid = uuid.uuid4()
        from_word_reference.save()

    # find word_reference using UUID and TO LANGUAGE.
    if WordReference.objects.filter(uuid=from_word_reference.uuid, language=to_language).exists():
        to_word_reference = WordReference.objects.get(uuid=from_word_reference.uuid, language=to_language)
    else:
        # word hasn't been found => crate_word.
        to_word_reference = _create_word_reference(from_language, to_language, word, from_word_reference.uuid)

    return to_word_reference