import uuid

from django.db import models
from core.models import Language

class WordReference(models.Model):
    """
    Word Reference => translate object.
    """
    uuid = models.UUIDField(blank=True, null=True)
    word = models.TextField()
    context = models.TextField(blank=True, null=True)
    language = models.ForeignKey(Language, on_delete=models.SET_NULL, blank=True, null=True)

    def __str__(self):
        return self.word



