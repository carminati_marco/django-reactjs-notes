const ROOT_URL = "http://localhost:8000/";
export const ROOT_LANGUAGE_URL = "http://localhost:8000/rest-note/languages/"


export const AuthUrls = {
    LOGIN: `${ROOT_URL}rest-registration/login/`,
    SIGNUP: `${ROOT_URL}rest-registration/register/`,
    CHANGE_PASSWORD: `${ROOT_URL}rest-registration/change-password/`,
    USER_PROFILE: `${ROOT_URL}rest-registration/profile/`
};

export const NoteUrls = {
    LIST: `${ROOT_URL}rest-note/notes/`,
    CREATE: `${ROOT_URL}rest-note/notes/`,
    READ: `${ROOT_URL}rest-note/notes/`,
    DELETE: `${ROOT_URL}rest-note/notes/`,
    UPDATE: `${ROOT_URL}rest-note/notes/`,
};
