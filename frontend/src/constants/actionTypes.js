{/* created cont ActionTypes for each models */}
export const AuthTypes = {
    LOGIN: "LOGIN",
    LOGOUT: "LOGOUT",
    CHANGE_PASSWORD: "CHANGE_PASSWORD",
    USER_PROFILE: "USER_PROFILE"
};

export const NoteTypes = {
    LIST: "LIST",
    CREATE: "CREATE",
    READ: "READ",
    UPDATE: "UPDATE",
};
