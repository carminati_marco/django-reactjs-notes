import axios from "axios";
import { SubmissionError } from "redux-form";
import history from "../utils/historyUtils";
import { actions as notifActions } from "redux-notifications";
const { notifSend } = notifActions;

import { NoteTypes } from "../constants/actionTypes";
import { NoteUrls } from "../constants/urls";
import store from "../store";
import { getUserToken } from "../utils/authUtils";

function setUserNote(payload) {
    return {
        type: NoteTypes.LIST,
        payload: payload
    };
}

export function getUserNotes() {
    // get data list of Notes for user
    return function(dispatch) {
        const token = getUserToken(store.getState());
        if (token) {
            axios.get(NoteUrls.LIST, {
                headers: {
                    authorization: "Token " + token
                }
            }).then(response => {
                dispatch(setUserNote(response.data))
            }).catch((error) => {
                dispatch(notifSend({
                    message: "We have some problems",
                    kind: "danger",
                    dismissAfter: 5000
                }));
            });
        }
    };
}

export function createNote(formValues, dispatch, props) {
        const createNoteUrl = NoteUrls.CREATE;
        const token = getUserToken(store.getState());
        if (token) {
          return axios.post(createNoteUrl, formValues, {
              headers: {
                  authorization: "Token " + token
              }
          }).then((response) => {
              history.push("/notes");
          }).catch(error => {
              const processedError = processServerError(error.response.data);
              throw new SubmissionError(processedError);
          });
          }
}


function setNote(payload) {
    return {
        type: NoteTypes.READ,
        payload: payload
    };
}

export function getNote(id) {
    return function(dispatch) {
        const url = `${NoteUrls.READ}${id}`;
        const token = getUserToken(store.getState());

        if (token) {
            axios.get(url, {
                headers: {
                    authorization: "Token " + token
                }
            }).then(response => {
                dispatch(setNote(response.data))
            }).catch((error) => {
                dispatch(notifSend({
                    message: "We have some problems",
                    kind: "danger",
                    dismissAfter: 5000
                }));
            });
        }
    };
}

export function deleteNote(id, callback){

  const url = `${NoteUrls.DELETE}${id}`;
  const token = getUserToken(store.getState());

  if (token) {
      axios.delete(url, {
          headers: {
              authorization: "Token " + token
          }
      }).then(response => {
          history.push(callback);
      }).catch((error) => {
          console.log(error)
      });
  }
}


export function updateNote(formValues, dispatch, props) {
        const { pk } = formValues;
        const url = `${NoteUrls.UPDATE}${pk}/`;
        const token = getUserToken(store.getState());
        if (token) {
          return axios.put(url, formValues, {
              headers: {
                  authorization: "Token " + token
              }
          }).then((response) => {
              history.push("/notes");
          }).catch(error => {
              const processedError = processServerError(error.response.data);
              throw new SubmissionError(processedError);
          });
          }
}
