import React from "react";
import { Switch, Route } from "react-router-dom";
import RequireAuth from "./auth/RequireAuth";
import Landing from "./Landing";
import Login from "./auth/Login";
import Logout from "./auth/Logout";
import Signup from "./auth/Signup";
import SignupDone from "./auth/SignupDone";
import UserProfile from "./auth/UserProfile";
import PasswordChange from "./auth/PasswordChange";
import NoMatch from "./NoMatch";
import NotesPage from "./notes/NotesPage";
import NoteDetail from "./notes/NoteDetail";
import NoteCreate from "./notes/NoteCreate";
import NoteEdit from "./notes/NoteEdit";

const MainContent = () => (
    <div className="container-fluid">
        <Switch>
            <Route exact path="/" component={Landing}/>
            <Route path="/login" component={Login}/>
            <Route path="/logout" component={Logout}/>
            <Route path="/signup" component={Signup}/>
            <Route path="/signup_done" component={SignupDone}/>
            <Route path="/profile" component={RequireAuth(UserProfile)}/>
            <Route path="/change_password" component={RequireAuth(PasswordChange)}/>
            <Route path="/notes" component={RequireAuth(NotesPage)} />
            <Route path="/notes-create" component={RequireAuth(NoteCreate)} />
            <Route path="/note-detail/:number" component={RequireAuth(NoteDetail)} />
            <Route path="/note-edit/:number" component={RequireAuth(NoteEdit)} />

            <Route component={NoMatch}/>
        </Switch>
    </div>
);

export default MainContent;
