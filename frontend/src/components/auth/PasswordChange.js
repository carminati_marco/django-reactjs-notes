import React, { Component } from "react";
import PropTypes from "prop-types";
import { reduxForm, Field, propTypes } from "redux-form";
import { required } from "redux-form-validators"
import { changePassword } from "../../actions/authActions";
import { renderError } from "../../utils/renderUtils";

import { TextField } from 'redux-form-material-ui'

class PasswordChange extends Component {

    static propTypes = {
        history: PropTypes.object
    };

    render() {
        const { handleSubmit, error } = this.props;

        return (
            <div className="justify-content-center col col-sm-12 mt-3 p-2">
                <h4 className="text-md-left">{"Change Password"}</h4>
                <hr/>
                <form className="col col-sm-6 p-2" onSubmit={handleSubmit} >
                    <div>
                        <Field name="old_password" floatingLabelText="Your actual password" component={TextField}
                               type="password" validate={[required({message: "This field is required."})]}
                        />
                    </div>

                    <div>
                        <Field name="password" floatingLabelText="New password" component={TextField}
                               type="password" validate={[required({message: "This field is required."})]}
                        />
                    </div>

                    <div>
                        <Field name="password_confirm" floatingLabelText="Confirm new password" component={TextField}
                               type="password" validate={[required({message: "This field is required."})]}
                        />
                    </div>

                    <div>
                        {renderError(error)}
                        <button action="submit" className="btn btn-primary mr-2">Submit</button>
                        <button className="btn btn-secondary" onClick={this.props.history.goBack}>Back</button>
                    </div>
                </form>
            </div>
        );
    }
}

// Sync field level validation for password match
const validateForm = values => {
    const errors = {};
    const { password, password_confirm } = values;
    if (password !== password_confirm) {
        errors.password_confirm = "Password does not match."
    }
    return errors;
};

export default reduxForm({
    form: "change_password",
    onSubmit: changePassword,
    validate: validateForm
})(PasswordChange);
