import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { logoutUser } from "../../actions/authActions";

class Logout extends Component {

    static propTypes = {
        logoutUser: PropTypes.func.isRequired
    };

    componentWillMount() {
        this.props.logoutUser();
    }

    render() {
        return (
          <div className="justify-content-center col col-sm-12 mt-3 p-2">
              <h4 className="text-md-left">{"Let us keep in touch!"}</h4>
              <hr />
              <p>{"We look forward to receive your new notes!"}</p>
          </div>
        );
    }
}

export default connect(null, { logoutUser })(Logout);
