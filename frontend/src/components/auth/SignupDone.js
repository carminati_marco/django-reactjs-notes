import React, { Component } from "react";

export default class SignupDone extends Component {
    render() {
        return (
          <div className="justify-content-center col col-sm-12 mt-3 p-2">
            <h4 className="text-md-left">Thanks for your registration</h4>
            <hr />
              <p>{"As this is a beta test, let's assume the address was checked with the email sent."}</p>
              <p>{"So"} <a href='/login' >login</a> {"and create your first note!"}</p>
          </div>
        )
    }
}
