import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import { getUserProfile } from "../../actions/authActions";
import { Card, CardActions, CardTitle, CardText } from "material-ui/Card";
import { List, ListItem } from "material-ui/List";
import Divider from "material-ui/Divider";

class UserProfile extends Component {

    static propTypes = {
        getUserProfile: PropTypes.func.isRequired,
        user: PropTypes.object
    };

    componentWillMount() {
        this.props.getUserProfile();
    }

    renderUser() {
        const {user } = this.props;
        if (user) {
            return (
                <Card>
                  <CardTitle title={user.username} />
                  <CardText>
                    <List>
                      <ListItem insetChildren={false} primaryText="Email" secondaryText={user.email} />
                    </List>
                    <Divider inset={false} />
                    <List>
                      <ListItem insetChildren={false} primaryText="First name" secondaryText={user.first_name} />
                      <ListItem insetChildren={false} primaryText="Last name" secondaryText={user.last_name} />
                    </List>
                  </CardText>
                  <div className="col p-2" >
                    <CardActions >
                      <Link className="btn btn-primary mr-2" to="/notes">My Notes</Link>
                      <Link className="btn btn-primary" to="/change_password">Change Password</Link>
                    </CardActions>
                  </div>
                </Card>
            );
        }
    }

    render() {
        return (
            <div className="justify-content-center col col-sm-12 mt-3 p-2" >
                {this.renderUser()}
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        user: state.auth.user
    }
}

export default connect(mapStateToProps, { getUserProfile } )(UserProfile);
