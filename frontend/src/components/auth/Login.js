import React, { Component } from "react";
import { reduxForm, Field, propTypes } from "redux-form";
import { Link } from "react-router-dom";
import { required } from "redux-form-validators"

import { renderError} from "../../utils/renderUtils";
import { loginUser } from "../../actions/authActions";

import { TextField } from 'redux-form-material-ui'

class Login extends Component {

    static propTypes = {
        ...propTypes
    };

    render() {
        const { handleSubmit, error } = this.props;

        return (
            <div className="justify-content-center col col-sm-12 mt-3 p-2">
              <h4 className="text-md-left">{"Log In and start create Notes"}</h4>
              <hr/>
              <form className="col col-sm-6 p-2"
                    onSubmit={handleSubmit}
                    >
                    <div>
                        <Field name="login" floatingLabelText="Username" component={TextField}
                               type="text" validate={[required({message: "This field is required."})]}
                        />
                    </div>

                    <div>
                        <Field name="password" floatingLabelText="Password" component={TextField}
                               type="password"  validate={[required({message: "This field is required."})]}
                        />
                    </div>

                    <div className="mt-3" >
                        { renderError(error) }
                        <button action="submit" className="btn btn-primary mt-2">Login</button>
                    </div>

                    <p>Not registered? <Link to="/signup">Signup Here!</Link></p>
              </form>
            </div>
        )
    }
}

export default reduxForm({
    form: "login",
    onSubmit: loginUser
})(Login);
