import React, { Component } from "react";
import axios from "axios";
// import PropTypes from "prop-types";
import { reduxForm, Field, propTypes } from "redux-form";
import { required } from "redux-form-validators"
import { renderError } from "../../utils/renderUtils";
import { signupUser } from "../../actions/authActions";

import { AutoComplete as MUIAutoComplete } from 'material-ui'
import { AutoComplete, TextField } from 'redux-form-material-ui'

import { ROOT_LANGUAGE_URL } from "../../constants/urls";

const dataSourceConfig = {
  text: 'textKey',
  value: 'valueKey',
};


class Signup extends Component {

    static propTypes = {
        ...propTypes
    };

    componentDidMount() {


      axios.get(ROOT_LANGUAGE_URL, {
      }).then(response => {

          let dataSourceLanguage = [];
          response.data.map(function (item) {
              dataSourceLanguage.push({ textKey: item.name, valueKey: item.pk, })
          });
          this.setState({ dataSourceLanguage: dataSourceLanguage });
      }).catch((error) => {
          // If request is bad...
          // Show an error to the user
          // TODO: send notification and redirect
      });
    };

    renderLanguageAutoField() {
        if (this.state && this.state.dataSourceLanguage) {
          return (<div>      <Field
                  name="language"
                  component={AutoComplete}
                  floatingLabelText="Your mother tongue"
                  openOnFocus
                  filter={MUIAutoComplete.fuzzyFilter}
                  dataSource={this.state.dataSourceLanguage}
                   dataSourceConfig={dataSourceConfig}
                /></div>)
        }
    }

    render() {
        const { handleSubmit, error } = this.props;
        return (
            <div className="justify-content-center col col-sm-12 mt-3 p-2">
                <h4 className="text-md-left">{"Register and start noting!"}"</h4>
                <hr/>
                <form className="col col-sm-6 p-2"
                    onSubmit={handleSubmit}
                >
                  { this.renderLanguageAutoField() }
                  <div>
                      <Field name="email" floatingLabelText="Email" component={TextField}
                       type="text" validate={[required({message: "This field is required."})]}/>
                  </div>

                  <div>
                      <Field name="first_name" floatingLabelText="First name" component={TextField}
                             type="text" validate={[required({message: "This field is required."})]}
                      />
                  </div>

                  <div>
                      <Field name="last_name" floatingLabelText="Last name" component={TextField}
                             type="text" validate={[required({message: "This field is required."})]}
                      />
                  </div>

                  <div>
                      <Field name="username" floatingLabelText="Username" component={TextField}
                             type="text" validate={[required({message: "This field is required."})]}
                      />
                  </div>

                  <div>
                      <Field name="password" floatingLabelText="Password" component={TextField}
                             type="password" validate={[required({message: "This field is required."})]}
                      />
                  </div>

                  <div>
                      <Field name="password_confirm" floatingLabelText="Confirm Password" component={TextField}
                             type="password" validate={[required({message: "This field is required."})]}
                      />
                  </div>

                  { renderError(error) }

                  <div className="mt-3" >
                      <button action="submit" className="btn btn-primary mt-2">Sign Up</button>
                  </div>
              </form>
          </div>
        )
        return null;
    }
}

// Sync field level validation for password match
const validateForm = values => {
    const errors = {};
    const { password, password_confirm, email } = values;

    if (password != password_confirm) {
        errors.password_confirm = "Password does not match."
    }

    if (email && ! email.match(/^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i)) {
            errors.email = "Insert a valid email address"
    }
    return errors;
};

export default reduxForm({
    form: "signup",
    validate: validateForm,
    onSubmit: signupUser
})(Signup);
