import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import { getUserNotes } from "../../actions/noteActions";
const {InputFilter, FilterResults} = fuzzyFilterFactory();

import fuzzyFilterFactory from 'react-fuzzy-filter';
import {List, ListItem} from 'material-ui/List';
import ContentInbox from 'material-ui/svg-icons/content/inbox';
import ActionGrade from 'material-ui/svg-icons/action/grade';
import ContentSend from 'material-ui/svg-icons/content/send';
import ContentDrafts from 'material-ui/svg-icons/content/drafts';
import Divider from 'material-ui/Divider';
import ActionInfo from 'material-ui/svg-icons/action/info';
import Subheader from 'material-ui/Subheader';


import FloatingActionButton from 'material-ui/FloatingActionButton';
import ContentAdd from 'material-ui/svg-icons/content/add';

const style = {
  marginRight: 20,
};

class NotesPage extends Component {

    static propTypes = {
        getUserNotes: PropTypes.func.isRequired,
        notes: PropTypes.object
    };

    componentWillMount() {
        this.props.getUserNotes();
    }

    renderLanguageUsers() {
        const language_users = this.props.language_users;

        const fuseConfig = {
          keys: ['word', 'word_reference'],
          threshold: 0,
          minMatchCharLength: 3,
        };


        if (language_users) {
            return (
              <div className="row justify-content-left col col-sm-12">
                  {language_users.map(function(language_user, index){
                    return (<div key={index} className="col col-sm-4 ">
                        <h5 key={ index } className="text-center column-header">{language_user.language}</h5>
                        <List>
                          <FilterResults
                            items={language_user.notes}
                            fuseConfig={fuseConfig}>
                            {filteredItems => {
                              return(
                                <div>

                                  {filteredItems.map(function(item, index_item) {
                                    return (
                                      <Link className="link-note" key={item.pk} to={`/note-detail/${item.pk}`}>
                                        <ListItem key={item.pk} primaryText={item.word}
                                        secondaryText={item.word_reference} />
                                        <Divider />
                                      </Link>
                                    )}
                                  )}
                                </div>
                              )
                            }}
                          </FilterResults>
                        </List>
                      </div>);
                  })}
              </div>
            );
        } return null;
    }

    render() {
        const inputProps = {
          placeholder:"Search"
        };

        return (
          <div className="justify-content-center col col-sm-12 mt-3 p-2">
            <div className="row"  >
              <div className="col col-sm-3">
              <h4 className="text-md-left">My Notes</h4>
            </div>
            <div className="col col-sm-3">
              <Link className="btn btn-primary mr-2" to="/notes-create">Add Note</Link>
            </div>
            </div>
            <hr/>
            <div className="row">
                    <div className="col-md-3 pull-right">
                        <InputFilter debounceTime={200} inputProps={inputProps}   />
                    </div>
            	</div>
              <hr/>
                {this.renderLanguageUsers()}
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        language_users: state.note.language_users,
    }
}

export default connect(mapStateToProps, { getUserNotes } )(NotesPage);
