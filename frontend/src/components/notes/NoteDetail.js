import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import { deleteNote, getNote } from "../../actions/noteActions";
import { Card, CardActions, CardTitle, CardText } from "material-ui/Card";
import { List, ListItem } from "material-ui/List";
import Divider from "material-ui/Divider";

class NoteDetail extends Component {

    static propTypes = {
        getNote: PropTypes.func.isRequired,
        note: PropTypes.object,
        history: PropTypes.object
    };

    componentWillMount() {
        this.props.getNote(this.props.match.params.number);
    }

    delete(){
      if (confirm('Delete the word?')) {
        deleteNote(this.props.note.pk, '/notes');
  		}
  	}

    renderNote() {
      const { note } = this.props;
      if (note) {
      return (
          <Card>
            <CardTitle title={note.word} subtitle={note.language} />
            <CardText>
              <List>
                <ListItem primaryText="Translation" secondaryText={note.word_reference} />
              </List>
              <Divider />
              <List>
                <ListItem primaryText="Definition" secondaryText={note.definition} />
                <ListItem primaryText="Context" secondaryText={note.context} />
              </List>
            </CardText>
            <div className="col p-2" >
              <CardActions >
                <Link className="btn btn-primary mr-2" to={`/note-edit/${note.pk}`}>Edit</Link>
                <button className="btn btn-primary mr-2" onClick={this.delete.bind(this)}>Delete</button>
                <button className="btn btn-secondary" onClick={this.props.history.goBack}>Back</button>
              </CardActions>
            </div>
          </Card>
        );
      }
    }

    render() {
      return (
          <div className="justify-content-center col col-sm-12 mt-3 p-2">
            {this.renderNote()}
          </div>
      )
    }
}

function mapStateToProps(state) {
    return {
        note: state.note.note,
    }
}

export default connect(mapStateToProps, { getNote } )(NoteDetail);
