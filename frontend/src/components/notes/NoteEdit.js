import React, { Component } from "react";
import PropTypes from "prop-types";
import { reduxForm, Field, propTypes } from "redux-form";
import { connect } from 'react-redux'
import { required } from "redux-form-validators"
import { renderHiddenField, renderError} from "../../utils/renderUtils";
import { updateNote, getNote } from "../../actions/noteActions";
import { TextField } from 'redux-form-material-ui'
import { Card, CardActions, CardTitle, CardText } from "material-ui/Card";
import { List, ListItem } from "material-ui/List";
import Divider from "material-ui/Divider";

class NoteEdit extends Component {

    static propTypes = {
        note: PropTypes.object,
        history: PropTypes.object
    };

    componentDidMount() {
      this.props.getNote(this.props.match.params.number)
  	}

    renderNote() {
      const { handleSubmit, error, note } = this.props;
      if (note) {
      return (
          <Card>
            <CardTitle title={note.word} subtitle={note.language} />
            <form onSubmit={handleSubmit} >
            <CardText>
                  <h5 className="text-purple" >{"Edit the word"}</h5>
                  <Field name="pk" component={renderHiddenField} type="hidden"  />
                  <Field name="word" component={renderHiddenField} type="hidden"  />

                  <div>
                      <Field name="definition" component={TextField} hintText="Definition"
                      floatingLabelText="Definition" multiLine
                      validate={[required({message: "This field is required."})]} />
                  </div>

                  <div>
                      <Field name="context" component={TextField} hintText="Context"
                      floatingLabelText="Context" multiLine rows={2}
                      validate={[required({message: "This field is required."})]} />
                  </div>

                  <div>
                      { renderError(error) }
                  </div>
              </CardText>
              <div className="col p-2" >
                <CardActions >
                  <button action="submit" className="btn btn-primary mr-2">Edit</button>
                  <button className="btn btn-secondary" onClick={this.props.history.goBack}>Back</button>
                </CardActions>
              </div>
            </form>
          </Card>
        );
      }
    }

    render() {
        return (
            <div className="justify-content-center col col-sm-12 mt-3 p-2">
              {this.renderNote()}
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {
        initialValues: state.note.note,
        note:state.note.note
    }
}

export default connect(mapStateToProps, { getNote})(reduxForm({
    form: "update_note",
    onSubmit: updateNote
})(NoteEdit));
