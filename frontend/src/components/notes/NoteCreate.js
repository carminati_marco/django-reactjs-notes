import React, { Component } from "react";
import axios from "axios";
import PropTypes from "prop-types";
import { reduxForm, Field, propTypes } from "redux-form";
import { required } from "redux-form-validators"

import { renderError } from "../../utils/renderUtils";
import { createNote } from "../../actions/noteActions";

import { AutoComplete as MUIAutoComplete } from 'material-ui'
import { AutoComplete, TextField } from 'redux-form-material-ui'

import { ROOT_LANGUAGE_URL } from "../../constants/urls";

const dataSourceConfig = {
  text: 'textKey',
  value: 'valueKey',
};

class NoteCreate extends Component {

    static propTypes = {
        history: PropTypes.object
    };

    componentDidMount() {
      axios.get(ROOT_LANGUAGE_URL, {
      }).then(response => {

          let dataSourceLanguage = [];
          response.data.map(function (item) {
              dataSourceLanguage.push({ textKey: item.name, valueKey: item.pk, })
          });
          this.setState({ dataSourceLanguage: dataSourceLanguage });
      }).catch((error) => {
          // If request is bad...
          // Show an error to the user
          // TODO: send notification and redirect
      });
    }

    renderLanguageAutoField() {
        if (this.state && this.state.dataSourceLanguage) {
          return (<div>      <Field
                  name="language"
                  component={AutoComplete}
                  floatingLabelText="Language"
                  openOnFocus
                  filter={MUIAutoComplete.fuzzyFilter}
                  dataSource={this.state.dataSourceLanguage}
                   dataSourceConfig={dataSourceConfig}
                /></div>)
        }
    }

    render() {
        const { handleSubmit, error } = this.props;
        return (
          <div className="justify-content-center col col-sm-12 mt-3 p-2">
            <h4 className="text-md-left">{"Add note"}</h4>
            <hr/>
            <form className="col col-sm-6 p-2" onSubmit={handleSubmit} >
                {this.renderLanguageAutoField()}
                <div>
                    <Field name="word" floatingLabelText="Word" component={TextField}
                           type="text" validate={[required({message: "This field is required."})]}
                    />
                </div>

                <div>
                    <Field name="definition" floatingLabelText="Definition" component={TextField}
                    type="definition" validate={[required({message: "This field is required."})]}
                    />
                </div>

                <div>
                    <Field name="context" floatingLabelText="Context" component={TextField}
                    multiLine rows={2} type="context"  validate={[required({message: "This field is required."})]}
                    />
                </div>

                <div className="mt-3" >
                    { renderError(error) }
                    <button action="submit" className="btn btn-primary mr-2">Add</button>
                    <button className="btn btn-secondary" onClick={this.props.history.goBack}>Back</button>
                    <p>Insert a new word, I will try to translate it in your language</p>
                </div>
            </form>
        </div>
    )}
}

export default reduxForm({
    form: "note",
    onSubmit: createNote
})(NoteCreate);
