import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";

class Landing extends Component {

    static propTypes = {
        authenticated: PropTypes.bool
    };

    render() {
      if (this.props.authenticated) {
          return (
            <div className="col col-sm-12 mt-3 p-2">
              <h4 className="text-md-left">Work with Noterise</h4>
              <hr/>
              <p>See your <a href="/notes" >notes</a>!</p>
              <p>Check and manage the <a href="/profile" >profile</a></p>
            </div>
          )
        } else {
          return (
            <div className="justify-content-center">
            <div className="justify-content-center col col-sm-12 mt-5 p-2">
                <h4 className="text-md-left">Hi, welcome to Noterise!</h4>
                <hr/>
                <p><a href="/login" >Login</a> and start to create notes!</p>
            </div>
            </div>
          )
        }
    }
}

function mapStateToProps(state) {
    return {
        authenticated: state.auth.authenticated
    }
}
export default connect(mapStateToProps)(Landing);
