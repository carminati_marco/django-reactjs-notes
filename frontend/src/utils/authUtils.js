{/* Utils for managing authentication. */}
export function getUserToken(state) {
    return state.auth.token;
}
