{/* Utils for render component. */}
import React from "react";

export const renderHiddenField = ({ input, type, meta: { touched, error } }) => (
    <input className="form-control" {...input} type={type}/>
);

export const renderError = (errorMessages) => {
    if ( errorMessages) {
        return (
            <div className="alert alert-danger">
                {errorMessages}
            </div>
        )
    }
};
