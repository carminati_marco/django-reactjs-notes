import "bootstrap/dist/css/bootstrap.css";
import 'redux-notifications/lib/styles.css';
import "./styles/style.css"
import React from "react";
import ReactDOM from "react-dom";
import { Router } from "react-router-dom";
import { Provider } from "react-redux";

import store from "./store";
import history from "./utils/historyUtils";
import { authLogin } from "./actions/authActions";
import App from "./components/App";

import {grey800, deepPurple500} from 'material-ui/styles/colors';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import AppBar from 'material-ui/AppBar';

// store token.
const token = localStorage.getItem("token");
if (token) {
    store.dispatch(authLogin(token));
}

// store mui-theme.
const muiTheme = getMuiTheme({
  palette: {
    textColor: grey800,
    primary1Color: deepPurple500
  },
  appBar: {
    height: 50,
  },
});


ReactDOM.render(
  <MuiThemeProvider muiTheme={muiTheme}>
    <Provider store={store}>
        <Router history={history}>
            <App />
        </Router>
    </Provider>
    </MuiThemeProvider>
    , document.getElementById("root"));
