import {combineReducers} from "redux";
import { reducer as formReducer } from "redux-form";
import { reducer as notifReducer } from 'redux-notifications';

{/* create reducer for each models */}
import authReducer from "./authReducer";
import noteReducer from "./noteReducer";

const rootReducer = combineReducers({
    form: formReducer,
    notifs: notifReducer,
    auth: authReducer,
    note: noteReducer
});

export default rootReducer;
