import { NoteTypes } from "../constants/actionTypes";

export default function(state = {}, action) {
    switch(action.type) {
        case NoteTypes.LIST:
            return { ...state, language_users: action.payload};
        case NoteTypes.CREATE:
            return { ...state, created: true, note: action.payload};
        case NoteTypes.READ:
            return { ...state, note: action.payload};
        case NoteTypes.UPDATE:
            return { ...state, updated: true, note: action.payload};            
    }
    return state;
}